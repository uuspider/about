---
layout: default
title: Knowledge, Experience & Opinion
---

*  [科学 & 技术](#scitech)
    *   [数学](#math)
    *   [物理](#physics)
*  [常识](#knowledge)

***

<h2 id="scitech">科学 & 技术</h2>

<h3 id="math">数学</h3>

- [为什么π 的平方约等于 g？](http://www.zhihu.com/question/21230794)

<h3 id="physics">物理</h3>

- [如何用大妈都能听懂的话介绍量子物理？](http://www.zhihu.com/question/22131485)

- [“简并”在物理学中有怎样的含义？这两种“简并”有什么关系？](http://www.zhihu.com/question/27824623)

- [为什么微观粒子具有不确定性？](http://www.zhihu.com/question/23510818)

- [物理学中的各种粒子，是如何通过各种实验证实存在的？](http://www.zhihu.com/question/26896927)

***

<h2 id="knowledge">常识</h2>

- [复印最常用的为什么是 A4 纸？](http://www.zhihu.com/question/21244313)

- [电子秤下垫泡沫就会增重吗？为什么？](http://www.zhihu.com/question/26577063)

